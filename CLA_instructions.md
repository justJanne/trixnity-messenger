# Contributor License Agreement Instructions

To sign the CLA you need to add a new line to [CLA_signers.txt](CLA_signers.txt).

The commit message has to be `I have read the CLA Document and I hereby sign the CLA`. It must be the first commit in
your merge request!

The new line must be in the following format:

```txt
<git-hoster>|<username>|<user-id>
```

For example:

```txt
gitlab.com|benkuly|3976820
```