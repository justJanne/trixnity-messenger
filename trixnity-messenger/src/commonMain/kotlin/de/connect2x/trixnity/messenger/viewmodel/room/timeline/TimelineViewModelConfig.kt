package de.connect2x.trixnity.messenger.viewmodel.room.timeline

interface TimelineViewModelConfig {
    val autoLoadBefore: Boolean
}
