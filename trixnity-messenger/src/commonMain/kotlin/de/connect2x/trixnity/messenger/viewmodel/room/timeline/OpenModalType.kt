package de.connect2x.trixnity.messenger.viewmodel.room.timeline

enum class OpenModalType {
    IMAGE,
    VIDEO,
}