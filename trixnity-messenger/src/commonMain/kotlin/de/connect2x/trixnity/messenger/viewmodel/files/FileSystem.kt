package de.connect2x.trixnity.messenger.viewmodel.files

import okio.FileSystem

expect fun getFileSystem(): FileSystem