package de.connect2x.trixnity.messenger

import kotlinx.coroutines.sync.Mutex

internal val accountMutex = Mutex()